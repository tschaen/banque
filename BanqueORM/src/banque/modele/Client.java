package banque.modele;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="client")
public class Client {
	
	public Client() {
	}

	public Client(Integer id, String nom, String prenom) {
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
	}

	@Id
	@Column(name="idclient")
	private Integer id;
	
	@Column(name="nomclient")
	private String nom;
	
	@Column(name="prenomclient")
	private String prenom;
	
	// Mapping des Comptes
	@ManyToMany
	@JoinTable(name="posseder", 
				joinColumns = @JoinColumn(name="idclient"),
				inverseJoinColumns = @JoinColumn(name="idcompte"))
	private List<Compte> lesComptes;

	public List<Compte> getLesComptes() {
		return lesComptes;
	}

	public void addLesComptes(Compte c) {
		this.lesComptes.add(c);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	@Override
	public String toString() {
		return "Client [id=" + id + ", nom=" + nom + ", prenom=" + prenom + "]";
	}
	
	

}
