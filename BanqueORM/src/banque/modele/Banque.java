package banque.modele;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="banque")
public class Banque {

	@Id
	@Column(name="idBanque")
	private int idBanque;
	
	private String nom;
	
	// OneToMany Banque -> Compte
	
	/*@OneToMany
	@JoinColumn(name="idbanque")
	private List<Compte> lesComptes = new ArrayList<Compte>();*/
	
	// mappedBy pour association biderectionnelle
	
	@OneToMany(mappedBy="banque")
	private List<Compte> lesComptes = new ArrayList<Compte>();
	
	public List<Compte> getLesComptes() {
		return lesComptes;
	}

	public void addLesComptes(Compte compte) {
		this.lesComptes.add(compte);
	}
	
/*	@Override
	public String toString() {
		String s = "Banque [idBanque=" + idBanque + ", nom=" + nom + "]";
		for(Compte c : lesComptes) {
			s = s + "\n compte : " + c;
		}
		return s;
	}	*/
	

	@Override
	public String toString() {
		return "Banque [idBanque=" + idBanque + ", nom=" + nom + "]";
	}	
	
	public Banque(int idBanque, String nom) {
		this.idBanque = idBanque;
		this.nom = nom;
	}

	public Banque() {
	}

	public int getIdBanque() {
		return idBanque;
	}

	public void setIdBanque(int idBanque) {
		this.idBanque = idBanque;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}


	
}
