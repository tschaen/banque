package banque.modele;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="compte")
public class Compte {

	@Id
	@Column(name="idcompte")
	private String idCompte;
	
	private int solde;
	
	
	// ManyToOne Compte -> Banque
	
	@ManyToOne
	@JoinColumn(name="idbanque")
	private Banque banque;
	
	@Override
	public String toString() {
		return "Compte [idCompte=" + idCompte + ", solde=" + solde + ", banque=" + banque + "]";
	}
	
	
//	@Override
//	public String toString() {
//		return "Compte [idCompte=" + idCompte + ", solde=" + solde + "]";
//	}
//		
	
	// Mapping des clients
	@ManyToMany(mappedBy="lesComptes")
	private List<Client> lesClients;
	
	public List<Client> getLesClients() {
		return lesClients;
	}

	public void addLesClients(Client c) {
		this.lesClients.add(c);
	}

	public Banque getBanque() {
		return banque;
	}

	public void setBanque(Banque banque) {
		this.banque = banque;
	}

	public Compte() {
	}

	public Compte(String idCompte, int solde) {
		this.idCompte = idCompte;
		this.solde = solde;
	}

	public String getIdCompte() {
		return idCompte;
	}

	public void setIdCompte(String idCompte) {
		this.idCompte = idCompte;
	}

	public int getSolde() {
		return solde;
	}

	public void setSolde(int solde) {
		this.solde = solde;
	}

	
	
	
}
