package banque.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;

public class TestJDBC {

	public static void main(String[] args) {
		// Test de connexion JDBC
		
		String jdbcURL = "jdbc:postgresql://192.168.222.86:5432/banque";
		String user = "tschaen";
		String password = "tschaen";
		
		try {
			System.out.println("Connexion à la BDD " + jdbcURL);
			Connection myConn = DriverManager.getConnection(jdbcURL, user, password);
			System.out.println("Connexion réussie.");
		}
		catch (Exception e) {
			// TODO: handle exception
			System.err.println("Echec de la connexion !!!");
			e.printStackTrace();
		}
		
	}

}
