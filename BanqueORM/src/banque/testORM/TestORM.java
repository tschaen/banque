package banque.testORM;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import banque.modele.Banque;
import banque.modele.Client;
import banque.modele.Compte;

public class TestORM {

	public static void main(String[] args) {

		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Client.class)
				.addAnnotatedClass(Banque.class)
				.addAnnotatedClass(Compte.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		
		session.beginTransaction();
		
		try {
			
			// Test OneToMany Banque -> Compte

			System.out.println("====== Test OneToMany Banque -> Compte ======");
			
			List<Banque> banques = session.createQuery("from Banque").getResultList();

			for(Banque b : banques) {
				System.out.println(b);
				
				for(Compte c : b.getLesComptes())
					System.out.println("\t"+c);
				
			}
			
			// Test ManyToOne Compte -> Banque
			
			System.out.println("====== Test ManyToOne Compte -> Banque ======");
						
			List<Compte> comptes = session.createQuery("from Compte").getResultList();

			for(Compte c : comptes) {
				System.out.println(c);
			}
			
			// Test ManyToMany Compte -> Client
			
			System.out.println("====== Test ManyToMany Compte -> Client ======");
			
			for(Compte compte : comptes) {
				System.out.println(compte);
				for (Client client : compte.getLesClients())
					System.out.println(client);
			}
			
			// Test liste des clients
			
			System.out.println("====== Test liste Client ======");
			
			List<Client> clients = session.createQuery("from Client").getResultList();

			for(Client client : clients) {
				System.out.println(client);
			}
			
			// Test ManyToMany Client -> Compte
			
			System.out.println("====== Test ManyToMany Client -> Compte ======");
			
			for(Client client : clients) {
				System.out.println(client);
				for (Compte compte : client.getLesComptes())
					System.out.println(compte);
			}
			

			session.getTransaction().commit();
			
		} catch (Exception e) {
			// Gestion des erreurs
			e.printStackTrace();
			session.getTransaction().rollback();
		}
		finally {
			//Fermeture propre
			factory.close();
		}
		
	}

}
